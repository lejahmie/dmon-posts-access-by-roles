<?php
/*
Plugin Name: Dmon Posts Access by Roles
Plugin URI:
Description: Adds access restriction to posts by user roles.
Author: Jamie Telin (DMON AB)
Author URI: http://www.dmon.se
Version: 1.3

	Copyright: © 2012 DMON AB (email : info@dmon.se)
	License: GNU General Public License v3.0
	License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/

if ( ! class_exists( 'Dmon_PostAccessByRoles' ) )
{
	class Dmon_PostAccessByRoles
	{
		public $post_types;
		public $options;
		public $options_saved;

		public function __construct()
		{
			// Load textdomain
			load_plugin_textdomain( 'Dmon_PostAccessByRoles', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

			if( is_admin() )
			{
				// Create meta boxes
				add_action( 'add_meta_boxes', array(&$this, 'add_custom_box' ) );
			}

			// Create option page and options
			require 'dmon-posts-access-by-roles-options.php';
			$this->options = new Dmon_PostAccessByRoles_Options();
			$this->options_saved = get_option( $this->options->_optionsName );

			// Setup post_types that plugin should be active on
			add_action( 'registered_post_type', array(&$this, 'add_default_post_types' ) );
			// Save postdata
			add_action( 'save_post', array(&$this, 'save_postdata' ) );
			// Filter out posts that should not be displayed based on user roles
			add_action( 'the_posts', array(&$this, 'restrict_the_posts' ) );
			// Filter out nav menu objects that should not be displayed based on user roles
			add_action( 'wp_nav_menu_objects', array(&$this, 'restrict_the_navigation' ) );

		}

		public function add_default_post_types()
		{
			$post_types = $this->get_post_types();

			foreach ($post_types as $key => $value) {
				if(!isset($this->options_saved['display_metabox_'.$value]) || empty($this->options_saved['display_metabox_'.$value]))
				{
					$this->options_saved['display_metabox_'.$value] = false;
				}
				
				if( !$this->options_saved['display_metabox_'.$value] ){
					unset($post_types[$key]);
				}
			}
			$this->post_types = apply_filters( 'Dmon_PostAccessByRoles_Post_Types',
				$post_types
			);
		}

		public function restrict_the_navigation( $objects ) {

			if( ! is_admin() )
			{
				//$this->d($objects);
				$menu_item_parent_to_delete_childs = array();
				foreach ($objects as $key => $object) {
					
					if( in_array( $object->object, $this->post_types) )
					{
						$roles = $this->get_roles();
						$hide = false;
						
						foreach($roles as $role => $name)
						{
							if( in_array($object->menu_item_parent, $menu_item_parent_to_delete_childs) )
							{
								$hide = true;
								break;
							}

							$meta_values[$role] = (boolean)get_post_meta($object->object_id, '_pabr_restrict_to_'.$role, true);

							if( $meta_values[$role] )
							{
								
								if( $role == $this->get_current_user_role() )
								{
									$hide = false;
									break;
								}
								else
								{
									$hide = true;
								}
							}
						}

						if( $hide )
						{
							$menu_item_parent_to_delete_childs[] = $object->ID;
							unset($objects[$key]);
						}
						
					}
				}

			}

			return $objects;
		}

		public function restrict_the_posts( $posts ) {

			if( ! is_admin() )
			{

				foreach ($posts as $key => $post) {

					if( in_array( $post->post_type, $this->post_types) )
					{

						$roles = $this->get_roles();
						$hide = false;

						$redirect_id = (int)get_post_meta($post->ID, '_pabr_redirect', true);
						$redirect_uri = false;
						if( $redirect_id > -1 && !empty( $redirect_id ) )
						{
							$redirect_uri = get_permalink( $redirect_id );
						}

						foreach($roles as $role => $name)
						{

							$meta_values[$role] = (boolean)get_post_meta($post->ID, '_pabr_restrict_to_'.$role, true);

							
							
							if( $meta_values[$role] )
							{
								if( $role == $this->get_current_user_role() )
								{
									$hide = false;
									break;
								}
								else
								{
									$hide = true;
								}
							}
						}

						if( $hide )
						{
							
							unset($posts[$key]);
							if( $redirect_uri )
							{
								wp_redirect( $redirect_uri, 302 ); exit;
							}
						}

					}


				}

			}

			return $posts;
		}

		/* Adds a box to the main column on the Post and Page edit screens */
		public function add_custom_box() {

		    foreach ($this->post_types as $post_type) {
		        add_meta_box(
		            'dmon-posts-restricted-by-roles',
		            apply_filters( 'Dmon_PostAccessByRoles_Meta_Box_Title', __( 'Restrict by roles', 'Dmon_PostAccessByRoles' ) ),
		            array( &$this, 'inner_custom_box' ),
		            $post_type,
		            'side',
		            'core'
		        );
		    }
		}

		/* Prints the box content */
		public function inner_custom_box( $post ) {

			// // Use nonce for verification
			wp_nonce_field( plugin_basename( __FILE__ ), '_pabr_nonce' );

			// The actual fields for data entry
			// Use get_post_meta to retrieve an existing value from the database and use the value for the form

			$roles = $this->get_roles();
			foreach($roles as $role => $name)
			{
				$value = get_post_meta( $post->ID, '_pabr_restrict_to_' . $role, true );
				?>

				<input <?php checked( (boolean)$value ); ?> type="checkbox" name="restrict-to-<?php echo $role; ?>" id="restrict-to-<?php echo $role; ?>" value="true">
				<label for="restrict-to-<?php echo $role; ?>"> <?php echo $name; ?> </label>
				<br />
				<?php
			}
			?>
			<p>
                  <span class="customize-page-list"><?php echo __('Redirect others to', 'Dmon_PostAccessByRoles'); ?></span><br />
                  <select style="width:99%;" name="pabr-redirect-to">
                    <?php
                    	$redirect_value = (int)get_post_meta( $post->ID, '_pabr_redirect', true );

	                	$list_pages_args = array(
							'sort_order' => 'ASC',
							'sort_column' => 'post_title',
							'hierarchical' => 1,
							'exclude' => '',
							'include' => '',
							'meta_key' => '',
							'meta_value' => '',
							'authors' => '',
							'child_of' => 0,
							'parent' => -1,
							'exclude_tree' => '',
							'number' => '',
							'offset' => 0,
							'post_type' => 'page',
							'post_status' => 'publish'
						);
						echo '<option value="-1"'.selected($redirect_value, -1).'> - ' . __('Select', 'Dmon_PostAccessByRoles') .' - </option>';
						$list_pages = get_pages( $list_pages_args );

						foreach ( $list_pages as $page_obj ) {
							$pre = '';

							if($page_obj->post_parent > 0){
								$depth = $this->get_page_depth($page_obj);
								for($i = 0; $i < $depth; $i++)
								{
									$pre .= '- ';
								}
							}

							$page_id_tmp = $page_obj->ID;

							echo '<option value="'.$page_obj->ID.'"'.selected($redirect_value, $page_obj->ID).'>' . $pre . $page_obj->post_title .'</option>';
						}
                    ?>
                  </select>
                </p>
			<?php
		}

		/* When the post is saved, saves our custom data */
		public function save_postdata( $post_id ) {


			if ( 'page' == $_POST['post_type'] ) {
				if ( ! current_user_can( 'edit_page', $post_id ) )
					return;
			} else {
				if ( ! current_user_can( 'edit_post', $post_id ) )
					return;
			}

			// // Secondly we need to check if the user intended to change this value.
			if ( ! isset( $_POST['_pabr_nonce'] ) || ! wp_verify_nonce( $_POST['_pabr_nonce'], plugin_basename( __FILE__ ) ) )
			{
				return;
			}

			// Thirdly we can save the value to the database

			//if saving in a custom table, get post_ID
			$post_ID = $_POST['post_ID'];
			//sanitize user input
			$roles = $this->get_roles();
			foreach($roles as $role => $name)
			{
				//Save data
				add_post_meta($post_ID, '_pabr_restrict_to_' . $role, (boolean)$_POST['restrict-to-' . $role], true) || update_post_meta($post_ID, '_pabr_restrict_to_' . $role, (boolean)$_POST['restrict-to-' . $role]);
			}
			//Save data
			add_post_meta($post_ID, '_pabr_redirect', (int)$_POST['pabr-redirect-to'], true) || update_post_meta($post_ID, '_pabr_redirect', (int)$_POST['pabr-redirect-to']);
			//clean_page_cache( $post_ID );

		}

		static function get_post_types()
		{
			$args=array(
			  'public'   => true,
			);
			$output = 'objects';
			$operator = 'and';
			$post_types = get_post_types($args,$output,$operator);

			$array = array();

			foreach ($post_types  as $key => $post_type ) {
				$array[] = $key;
			}

			return $array;
		}

		static function get_post_type_names()
		{
			$args=array(
			  'public'   => true,
			);
			$output = 'objects';
			$operator = 'and';
			$post_types = get_post_types($args,$output,$operator);

			$array = array();

			foreach ($post_types  as $key => $post_type ) {
				$array[$key] = $post_type->labels->name;
			}

			return $array;
		}

		public function get_current_user_role()
		{
			if( ! is_user_logged_in() ) { return; }

			global $wpdb;
			$user_ID = get_current_user_id();
			$user = get_userdata( $user_ID );

			$capabilities = $user->{$wpdb->prefix . 'capabilities'};

			$roles = $this->get_roles();

			foreach($roles as $role => $name)
			{

				if ( array_key_exists( $role, $capabilities ) )
				{
					return $role;
				}

			}

		}

		public function get_roles()
		{
				if ( !isset( $wp_roles ) )
				{
					$wp_roles = new WP_Roles();
				}

				$roles = array();

				foreach ( $wp_roles->role_names as $role => $name ) {
					$roles[$role] = translate_user_role($name);
				}

				return $roles;
		}

		public function get_page_depth($page = false){

			if(!$page)
			{
				$page = get_page(get_the_ID());
			}

			$parent_id  = $page->post_parent;
			$depth = 0;
			while($parent_id > 0){
				$page = get_page($parent_id);
				$parent_id = $page->post_parent;
				$depth++;
			}

		 	return $depth;
		}

		/**
		* Check if user is Super Admin
		*/
		public function isSuperAdmin()
		{

			$current_user = wp_get_current_user(); $current_user_id = $current_user->ID; // get the user ID

			if($current_user_id == '1') {
				return true;
			} else {
				return false;
			}

		}

		/**
		* Quick alias for war dumping
		*/
		static function d($data)
		{
			if( !empty($data) )
			{
				print "<pre>";
				var_dump( $data );
				print "</pre>";
			}
		}

	}

	// finally instantiate our plugin class and add it to the set of globals
	$GLOBALS['dmon_PostAccessByRoles'] = new Dmon_PostAccessByRoles();
}
