=== Dmon Posts Access by Roles ===
Contributors: Jamie Telin (DMON AB)
Tags: admin
Version: 1.3

== Description ==

Restrict acess till posts and pages based on roles.

== Installation ==

1. Upload the entire 'dmon-posts-restricted-by-roles' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 1.0.0 =
* Initial Release
