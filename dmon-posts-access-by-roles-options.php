<?php
if ( ! class_exists( 'Dmon_PostAccessByRoles_CoreOptions' ) )
{
	class Dmon_PostAccessByRoles_CoreOptions {

		public $_optionsName;

		/* Initialize */
		function __construct() {

			$this->_optionsName = 'Dmon_PostAccessByRoles_Options';

		}

		public function default_section_fn( $string ) {
			echo $string;
		}

		/**
		* CONTROLL FUNCIONS
		**/
		public function setting_dropdown_fn( $newargs ) {
			$options = get_option( $this->_optionsName );

			$args = array(
					'id' => 'dropdown',
					'items' => array(),
				);
			$args = array_merge($args, $newargs);

			$items = $args['items'];

			echo "<select id='".$args['id']."' name='".$this->_optionsName."[".$args['id']."]'>";
			foreach($items as $item) {
				$selected = ($options[$args['id']]==$item) ? 'selected="selected"' : '';
				echo "<option value='".$item."' ".$selected.">".$item."</option>";
			}
			echo "</select>";
		}

		public function setting_textarea_fn( $newargs ) {
			$options = get_option( $this->_optionsName );

			$args = array(
					'id' => 'textarea',
				);
			$args = array_merge($args, $newargs);

			echo "<textarea id='".$args['id']."' name='".$this->_optionsName."[".$args['id']."]' rows='7' cols='50' type='textarea'>".$options[$args['id']]."</textarea>";
		}

		public function setting_textfield_fn( $newargs ) {
			$options = get_option( $this->_optionsName );

			$args = array(
					'id' => 'textfield',
				);
			$args = array_merge($args, $newargs);

			echo "<input id='".$this->_optionsName.'_'.$args['id']."' name='".$this->_optionsName."[".$args['id']."]' size='40' type='text' value='".$options[$args['id']]."' />";
		}

		public function setting_password_fn( $newargs ) {
			$options = get_option( $this->_optionsName );

			$args = array(
					'id' => 'password',
				);
			$args = array_merge($args, $newargs);

			echo "<input id='".$this->_optionsName.'_'.$args['id']."' name='".$this->_optionsName."[".$args['id']."]' size='40' type='password' value='".$options[$args['id']]."' />";
		}

		public function setting_checkbox_fn( $newargs ) {
			$options = get_option( $this->_optionsName );
			
			
			$args = array(
					'id' => 'checkbox',
				);
			$args = array_merge($args, $newargs);

			if(!isset($options[ $args['id'] ])){
				$options[ $args['id'] ] = false;
			}

			echo "<input ".checked( $options[ $args['id'] ], true, false )." value='1' id='".$this->_optionsName.'_'.$args['id']."' name='".$this->_optionsName."[".$args['id']."]' type='checkbox' />";
		}

		public function setting_radio_fn( $newargs ) {
			$options = get_option( $this->_optionsName );

			$args = array(
					'id' => 'radio',
					'items' => array(),
				);
			$args = array_merge($args, $newargs);

			if(!isset($options[ $args['id'] ])){
				$options[ $args['id'] ] = false;
			}

			$items = $args['items'];

			foreach($items as $item) {
				echo "<label><input ".checked( $options[ $args['id'] ], $item, false )." value='".$item."' name='".$this->_optionsName."[".$args['id']."]' type='radio' /> $item</label><br />";
			}
		}
	}
}
if ( ! class_exists( 'Dmon_PostAccessByRoles_Options' ) )
{
	class Dmon_PostAccessByRoles_Options extends Dmon_PostAccessByRoles_CoreOptions {

		/* Initialize */
		function __construct() {

			parent::__construct();

			// Add sub page to the Settings Menu
			add_action('admin_menu', array(&$this, 'add_page_fn') );
			// Register our settings. Add the settings section, and settings fields
			add_action('admin_init', array(&$this, 'init_fn' ) );
			// Define default option settings
			register_activation_hook(__FILE__, array(&$this, 'add_defaults_fn' ) );
			register_deactivation_hook(__FILE__, array(&$this, 'deactivation_fn' ) );
		}

		// Define default option settings
		public function deactivation_fn() {
		}

		// Define default option settings
		public function add_defaults_fn() {
		    $arr = array(
		    	'display_metabox_page' => 1,
		    	'display_metabox_post' => 1,
		    	);
		    update_option($this->_optionsName, $arr);
		}

		// Register our settings. Add the settings section, and settings fields
		public function init_fn(){

			register_setting($this->_optionsName, $this->_optionsName, false );

			// Add sections
			add_settings_section('diaplay_metabox_section', __('Display on', 'Dmon_PostAccessByRoles') ,
				function() {
					_e('Select what post types should display the settings.', 'Dmon_PostAccessByRoles'); echo '<br /><i>'; _e('Post types are loaded automaticly from WordPress and there is no gurantee that it works on all.', 'Dmon_PostAccessByRoles'); echo '</i>';
				},
				 __FILE__);

			//Add fields: general_section
			$post_types = Dmon_PostAccessByRoles::get_post_type_names();
			foreach ($post_types as $key => $post_type ) {
				add_settings_field('display_metabox_'.$key,
				$post_type,
				array(&$this, 'setting_checkbox_fn'),
				__FILE__,
				'diaplay_metabox_section',
				array(
					'id' => 'display_metabox_'.$key,
					)
				);
			}
		}

		// Add sub page to the Settings Menu
		public function add_page_fn() {
			add_options_page(__('Posts Access by Roles', 'Dmon_PostAccessByRoles'), __('Posts Access by Roles', 'Dmon_PostAccessByRoles'), 'administrator', __FILE__, array(&$this, 'options_page_fn') );
		}

		public function options_page_fn() {
		?>
			<div class="wrap">
				<div class="icon32" id="icon-options-general"><br></div>
				<h2><?php  _e('Posts Access by Roles', 'Dmon_PostAccessByRoles'); ?></h2>
				<p><?php _e('Posts Access by Roles allows you to restrict access to posts and pages based on user roles.', 'Dmon_PostAccessByRoles'); ?></p>
				<form action="options.php" method="post">
				<?php settings_fields( $this->_optionsName ); ?>
				<?php do_settings_sections( __FILE__ ); ?>
				<p class="submit">
					<input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
				</p>
				</form>
			</div>
		<?php
		}

	}
}
?>