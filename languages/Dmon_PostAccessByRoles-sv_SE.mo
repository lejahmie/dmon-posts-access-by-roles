��    	      d      �       �   
   �   _   �      L  [   b     �     �     �  3   �  �       �  c   �     U  t   o     �     �       6                            	                     Display on Post types are loaded automaticly from WordPress and there is no gurantee that it works on all. Posts Access by Roles Posts Access by Roles allows you to restrict access to posts and pages based on user roles. Redirect others to Restrict by roles Select Select what post types should display the settings. Project-Id-Version: Dmon Posts Access by Roles
POT-Creation-Date: 2013-06-13 15:05+0100
PO-Revision-Date: 2013-06-13 15:07+0100
Last-Translator: Jamie telin <jamie.telin@dmon.se>
Language-Team: DMON AB <info@dmon.se>
Language: Svenska
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.5
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
 Visa på Posttyper laddas automatiskt från WordPress och det finns ingen garanti att det fungerar på alla. Poståtkomst efter roller Med "poståtkomst efter roller" kan du begränsa tillgången till inlägg och sidor baserat på användarnas roller. Omdirigering andra till Begränsa efter roller Välj Välj vilka posttyper som skall visa inställningarna. 